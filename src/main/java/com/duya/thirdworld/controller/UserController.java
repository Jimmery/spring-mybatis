package com.duya.thirdworld.controller;

import com.duya.thirdworld.service.UserService;
import com.duya.thirdworld.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping(value = "/getUserInfo")
    public Map<String, Object> getUserInfo(Integer userId) {
        return userService.getUserMsg(userId);
    }

}
