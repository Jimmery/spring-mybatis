package com.duya.thirdworld.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface UserMapper {

    // 获取用户信息
    Map<String, Object> getUserInfo(Integer userId);
}
