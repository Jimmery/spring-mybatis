package com.duya.thirdworld.Entity;

import lombok.Data;

@Data
public class User {

    private Integer userId;

    private String password;

    private String username;
}
