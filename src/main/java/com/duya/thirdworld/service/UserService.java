package com.duya.thirdworld.service;

import com.duya.thirdworld.mapper.UserMapper;
import com.duya.thirdworld.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserService implements UserServiceImpl {

    // mapper
    @Autowired
    private UserMapper userMapper;

    @Override
    public Map<String, Object> getUserMsg(Integer userId) {
        return userMapper.getUserInfo(userId);
    }
}
