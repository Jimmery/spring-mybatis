package com.duya.thirdworld.service.impl;

import java.util.Map;

public interface UserServiceImpl {

    Map<String, Object> getUserMsg(Integer userId);

}
