package com.duya.thirdworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThirdworldApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdworldApplication.class, args);
    }
}
